<?php

/**
 * @file
 * Map drupal entities to other entities and convert between them.
 */

/**
 * @param $name
 * @param $from_entity_type
 * @param $to_entity_type
 * @return EntityMapper
 */
function entity_mapper($name, $from_entity_type, $to_entity_type) {
  return new EntityMapper($name, $from_entity_type, $to_entity_type);
}

/**
 * @return array
 */
function entity_mapper_get_mappers() {
  $mappers = array();
  foreach (module_implements('entity_mapper_mappers') as $module) {
    $function = $module . '_entity_mapper_mappers';
    $mappers += $function();
  }

  return $mappers;
}


/**
 * @param $mapper_name
 * @param $entity_type
 * @param $entity_id
 * @return bool|EntityMetadataWrapper
 */
function entity_mapper_get_mapped_entity($mapper_name, $entity_type, $entity_id) {
  $result = db_select('entity_mapper_mapping', 'mp')->fields('mp')
    ->condition('mapper', $mapper_name)
    ->condition('from_entity_type', $entity_type)
    ->condition('from_entity_id', $entity_id)
    ->execute()->fetch();
  if (!empty($result)) {
    return entity_metadata_wrapper($result->to_entity_type, entity_load_single($result->to_entity_type, $result->to_entity_id));
  }
  return FALSE;
}

/**
 * @param $entity
 * @param $type
 */
function entity_mapper_entity_delete($entity, $type) {
  // Clean up after ourselves.
  db_delete('entity_mapper_mapping')
    ->condition('to_entity_type', $type)
    ->condition('to_entity_id', entity_id($type, $entity))
    ->execute();
  db_delete('entity_mapper_mapping')
    ->condition('from_entity_type', $type)
    ->condition('from_entity_id', entity_id($type, $entity))
    ->execute();
}

/**
 * @param $mapper_name
 * @param $from_entity
 * @param $to_entity
 */
function entity_mapper_save_mapping($mapper_name, $from_entity, $to_entity) {
  $record = array(
    'mapper' => $mapper_name,
    'from_entity_type' => $from_entity->type(),
    'from_entity_id' => $from_entity->getIdentifier(),
    'to_entity_type' => $to_entity->type(),
    'to_entity_id' => $to_entity->getIdentifier(),
  );
  drupal_write_record('entity_mapper_mapping', $record);
}
