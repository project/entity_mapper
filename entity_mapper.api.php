<?php

/**
 * @file
 * Hooks provided by the entity_mapper API.
 */


/**
 * Define the mappers provided by your module in this hook.
 * Modules depending on entity mapper can then use the defined mappers
 * using entity_mapper_get_mappers().
 *
 * @return array
 *  - mappersetname: the name of the set of mappers to use
 *  - label: The label of the mapper set
 *  - mappers: A keyed array with mappers, keyed with the entity to map from.
 */
function hook_entity_mapper_mappers() {
  $mapper = entity_mapper('user2node', 'user', 'node')
    ->setDefaults(array('type' => 'profile'))
    ->addMapping(new PropertyMapper('name', 'title'));

  return array(
    'mappersetname' => array(
      'label' => 'My mapper set name',
      'mappers' => array(
        'user' => $mapper,
      ),
    ),
  );
}