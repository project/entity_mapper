<?php

/**
 * @file
 * Map drupal entities to other entities and convert between them.
 */

/**
 * Class EntityMapper
 */
class EntityMapper {
  protected $mappers, $fromEntityType, $toEntityType;

  /**
   * @param $name
   * @param $fromEntityType
   * @param $toEntityType
   */
  function __construct($name, $fromEntityType, $toEntityType) {
    $this->name = $name;
    $this->mappers = array();
    $this->formatters = array();
    $this->fromEntityType = $fromEntityType;
    $this->toEntityType = $toEntityType;
    $this->defaults = array();
  }

  /**
   * @param $defaults
   * @return $this
   */
  function setDefaults($defaults) {
    $this->defaults = $defaults;
    return $this;
  }

  /**
   * @return mixed
   */
  function getFromEntityType() {
    return $this->fromEntityType;
  }

  /**
   * @return mixed
   */
  function getToEntityType() {
    return $this->toEntityType;
  }

  /**
   * @param PropertyMapper $mapper
   * @return $this
   */
  function addMapping(PropertyMapper $mapper) {
    $mapper->setEntityMapper($this);
    $this->mappers[] = $mapper;
    return $this;
  }

  /**
   * @param $formatter
   * @return $this
   */
  function addFormatter($formatter) {
    $this->formatters[] = $formatter;
    return $this;
  }

  /**
   * @param EntityMetadataWrapper $fromEntity
   * @param EntityMetadataWrapper $toEntity
   * @return bool|EntityMetadataWrapper
   */
  function map(EntityMetadataWrapper $fromEntity, EntityMetadataWrapper $toEntity = NULL) {
    if (empty($toEntity)) {
      $toEntity = entity_mapper_get_mapped_entity($this->name, $fromEntity->type(), $fromEntity->getIdentifier());
      if (empty($toEntity)) {
        $toEntity = entity_metadata_wrapper($this->toEntityType, entity_create($this->toEntityType, $this->defaults));
      }
    }
    foreach ($this->mappers as $mapper) {
      $mapper->map($fromEntity, $toEntity);
    }
    foreach ($this->formatters as $formatter) {
      $formatter($fromEntity, $toEntity);
    }
    $toEntity->save();
    entity_mapper_save_mapping($this->name, $fromEntity, $toEntity);
    return $toEntity;
  }
}

/**
 * Class ConditionalEntityMapper
 */
class ConditionalEntityMapper extends EntityMapper {
  /**
   * @param $mappers
   * @param $selectionFn
   */
  function __construct($mappers, $selectionFn) {
    $this->mappers = $mappers;
    $this->selectionFn = $selectionFn;
  }

  /**
   * @param EntityMetadataWrapper $fromEntity
   * @param EntityMetadataWrapper $toEntity
   * @return bool|EntityMetadataWrapper|null
   */
  function map(EntityMetadataWrapper $fromEntity, EntityMetadataWrapper $toEntity = NULL) {
    $fn = $this->selectionFn;
    $mapper = $fn($this->mappers, $fromEntity, $toEntity);
    if ($mapper) {
      return $mapper->map($fromEntity, $toEntity);
    }
    return NULL;
  }
}

/**
 * Class FileEntityMapper
 */
class FileEntityMapper extends EntityMapper {
  /**
   * @param $name
   * @param string $targetDir
   */
  function __construct($name, $targetDir = 'public://') {
    $this->targetDir = $targetDir;
    $this->name = $name;
  }

  /**
   * @param EntityMetadataWrapper $fromEntity
   * @param EntityMetadataWrapper $toEntity
   * @return bool|EntityMetadataWrapper|null
   */
  function map(EntityMetadataWrapper $fromEntity, EntityMetadataWrapper $toEntity = NULL) {
    $uri = $fromEntity->get('uri')->value();
    $name = $fromEntity->get('name')->value();
    $contents = file_get_contents($uri);
    if (!empty($contents)) {
      $file = file_save_data($contents, $this->targetDir . '/' . $name, FILE_EXISTS_REPLACE);
      if (!empty($file)) {
        $wrapper = entity_metadata_wrapper('file', $file);
        entity_mapper_save_mapping($this->name, $fromEntity, $wrapper);
        return $wrapper;
      }
    }
    return NULL;
  }
}