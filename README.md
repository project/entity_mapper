# Entity mapper

Entity mapper is a module that maps entity properties on different types
of entities to each other. It is used when you want to convert an entity
of a particular type to another type. You might for instance have a remote
entity that you want to fetch and then map to a node of a particular type.

## Usage

TBD
