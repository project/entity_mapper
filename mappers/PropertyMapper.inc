<?php

/**
 * @file
 * Map drupal entities to other entities and convert between them.
 */

/**
 * Class PropertyMapper
 */
class PropertyMapper {
  private $entityMapper;

  /**
   * @param $fromProperty
   * @param $toProperty
   */
  function __construct($fromProperty, $toProperty) {
    $this->formatters = array();
    $this->toProperty = $toProperty;
    $this->fromProperty = $fromProperty;
  }

  /**
   * @param $fromEntity
   * @param bool $asWrapper
   * @return null|object
   */
  function getFromValue($fromEntity, $asWrapper = FALSE) {
    $selectors = explode('.', $this->fromProperty);
    $first_selector = array_shift($selectors);
    $property = $fromEntity->get($first_selector);
    $entityValue = $fromEntity->value();
    if (empty($entityValue)) {
      return;
    }
    $value = $property->value();
    if (!isset($value) || (!is_object($value) && !empty($selectors))) {
      return NULL;
    }
    foreach ($selectors as $selector) {
      // If this entity wrapper is a structure, we might be able to get properties
      // by using getters and setters.
      if (method_exists($property, 'getPropertyInfo')) {
        $propertyInfo = $property->getPropertyInfo();
      }
      else {
        $property = NULL;
      }
      if (!empty($property) && !empty($propertyInfo[$selector])) {
        $value = $property->value();
        if (isset($value)) {
          $property = $property->get($selector);
        }
        else {
          return;
        }
      }
      else {
        if (empty($value->{$selector})) {
          return NULL;
        }
        $value = $value->{$selector};
      }
    }

    return $asWrapper ? $property : $value;
  }

  /**
   * @param $toEntity
   * @param $value
   */
  function setToValue($toEntity, $value) {
    $selectors = explode('.', $this->toProperty);
    $first_selector = array_shift($selectors);
    if (empty($selectors)) {
      $toEntity->get($first_selector)->set($value);
      return;
    }
    $last_selector = array_pop($selectors);
    $obj = $toEntity->get($first_selector)->value();
    foreach ($selectors as $selector) {
      $obj = (object)$obj->{$selector};
    }
    $obj->{$last_selector} = $value;
  }

  /**
   *
   * @param EntityMetadataWrapper $fromEntity
   * @param EntityMetadataWrapper $toEntity
   *
   * @return EntityDrupalWrapper
   */
  function map(EntityMetadataWrapper $fromEntity, EntityMetadataWrapper $toEntity) {
    $value = $this->getFromValue($fromEntity);
    if (isset($value)) {
      $this->setToValue($toEntity, $this->applyFormatters($value));
    }
  }

  /**
   * @param $formatter
   * @return $this
   */
  function addFormatter($formatter) {
    $this->formatters[] = $formatter;
    return $this;
  }

  /**
   * @param $value
   * @return mixed
   */
  function applyFormatters($value) {
    foreach ($this->formatters as $formatter) {
      $value = $formatter($value);
    }
    return $value;
  }

  /**
   * @return mixed
   */
  function getEntityMapper() {
    return $this->entityMapper;
  }

  /**
   * @param EntityMapper $mapper
   */
  function setEntityMapper(EntityMapper $mapper) {
    $this->entityMapper = $mapper;
  }

  /**
   * @return mixed
   */
  function getFromEntityType() {
    return $this->entityMapper->getFromEntityType();
  }

  /**
   * @return bool
   */
  function getToEntityType() {
    return $this->entityMapper > getFromEntityType();
  }
}

/**
 * Class EntityListPropertyMapper
 */
class EntityListPropertyMapper extends PropertyMapper {
  /**
   * @param $fromProperty
   * @param $toProperty
   * @param EntityMapper $mapper
   */
  function __construct($fromProperty, $toProperty, EntityMapper $mapper) {
    parent::__construct($fromProperty, $toProperty);
    $this->mapper = $mapper;
  }

  /**
   * @param EntityMetadataWrapper $fromEntity
   * @param EntityMetadataWrapper $toEntity
   * @return EntityDrupalWrapper|void
   */
  function map(EntityMetadataWrapper $fromEntity, EntityMetadataWrapper $toEntity) {
    $newList = array();
    foreach ($fromEntity->get($this->fromProperty)->getIterator() as $index => $item) {
      $toProperty = $toEntity->get($this->toProperty);
      if (!empty($toProperty)) {
        $toItem = $toProperty->get($index);
      }
      $value = $toItem->value();
      if (empty($toItem) || empty($value)) {
        $toItem = NULL;
      }
      $mappedItem = $this->mapper->map($item, $toItem);
      if (!empty($mappedItem)) {
        $mappedItem->save();
        $newList[] = $mappedItem->value();
      }
    }
    $toEntity->get($this->toProperty)->set($newList);
  }
}

/**
 * Class EntityPropertyMapper
 */
class EntityPropertyMapper extends PropertyMapper {
  /**
   * @param $fromProperty
   * @param $toProperty
   * @param EntityMapper $mapper
   */
  function __construct($fromProperty, $toProperty, EntityMapper $mapper) {
    parent::__construct($fromProperty, $toProperty);
    $this->mapper = $mapper;
  }

  /**
   * @param $value
   * @return mixed
   */
  function mapEntity($value) {
    $mappedItem = $this->mapper->map($value);
    $mappedItem->save();
    return $mappedItem->value();
  }

  /**
   * @param EntityMetadataWrapper $fromEntity
   * @param EntityMetadataWrapper $toEntity
   * @return EntityDrupalWrapper|void
   */
  function map(EntityMetadataWrapper $fromEntity, EntityMetadataWrapper $toEntity) {
    $value = $this->getFromValue($fromEntity, TRUE);
    if (!empty($value)) {
      $this->setToValue($toEntity, $this->mapEntity($value));
    }
  }
}

/**
 * Class FileListPropertyMapper
 */
class FileListPropertyMapper extends PropertyMapper {
  /**
   * @param $fromProperty
   * @param $toProperty
   * @param string $targetDir
   */
  function __construct($fromProperty, $toProperty, $targetDir = 'public://') {
    parent::__construct($fromProperty, $toProperty);
    $this->targetDir = $targetDir;
  }

  /**
   * @param EntityMetadataWrapper $fromEntity
   * @param EntityMetadataWrapper $toEntity
   * @return EntityDrupalWrapper|void
   */
  function map(EntityMetadataWrapper $fromEntity, EntityMetadataWrapper $toEntity) {
    $newList = array();
    $property = $this->getFromValue($fromEntity, $this->fromProperty, TRUE);

    if (empty($property)) {
      return;
    }
    $info = $property->info();
    if (strstr($info['type'], 'list') !== FALSE) {
      $iterator = $property->getIterator();
    }
    else {
      $iterator = array($property);
    }
    foreach ($iterator as $wrapper) {
      $value = $wrapper->value();
      if (!empty($value)) {
        $uri = $wrapper->get('uri')->value();
        $name = $wrapper->get('name')->value();
        if (!empty($uri)) {
          $contents = file_get_contents($uri);
          if (!empty($contents)) {
            $file = file_save_data($contents, $this->targetDir . '/' . $name, FILE_EXISTS_REPLACE);
            if (!empty($file)) {
              $file->display = 1;
              $newList[] = (array)$file;
            }
          }
        }
      }
    }
    $toEntity->get($this->toProperty)->set($newList);
  }
}

/**
 * Class FilePropertyMapper
 */
class FilePropertyMapper extends PropertyMapper {
  /**
   * @param $fromProperty
   * @param $toProperty
   * @param string $targetDir
   */
  function __construct($fromProperty, $toProperty, $targetDir = 'public://') {
    parent::__construct($fromProperty, $toProperty);
    $this->targetDir = $targetDir;
  }

  /**
   * @param EntityMetadataWrapper $fromEntity
   * @param EntityMetadataWrapper $toEntity
   * @return EntityDrupalWrapper|void
   */
  function map(EntityMetadataWrapper $fromEntity, EntityMetadataWrapper $toEntity) {
    $wrapper = $this->getFromValue($fromEntity, TRUE);
    if (empty($wrapper)) {
      return;
    }
    $value = $wrapper->value();
    if (!empty($value)) {
      $uri = $wrapper->get('uri')->value();
      $name = $wrapper->get('name')->value();
      $contents = file_get_contents($uri);
      if (!empty($contents)) {
        $file = file_save_data($contents, $this->targetDir . '/' . $name);
        $this->setToValue($toEntity, $file);
      }
    }
  }
}

/**
 * Class StaticPropertyMapper
 */
class StaticPropertyMapper extends PropertyMapper {
  /**
   * @param $fromProperty
   * @param $value
   */
  function __construct($fromProperty, $value) {
    $this->fromProperty = $fromProperty;
    $this->value = $value;
  }

  /**
   * @param EntityMetadataWrapper $fromEntity
   * @param EntityMetadataWrapper $toEntity
   * @return EntityDrupalWrapper|void
   */
  function map(EntityMetadataWrapper $fromEntity, EntityMetadataWrapper $toEntity) {
    $toEntity->set($this->toProperty, $this->value);
  }
}